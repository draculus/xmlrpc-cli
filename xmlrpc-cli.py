#!/usr/bin/env python
# -*- coding: utf-8 -*-

# XML-RPC Client -- simple GTK frontend to communicate with XML-RPC services.
# Copyright (C) 2013  Pavel Kacer <pavel.kacer@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

import xmlrpclib
import time
import pygtk
pygtk.require('2.0')
import gtk
import gtk.glade

class rpchelper:

    def call_server(self, uri, method, params):
        server = xmlrpclib.Server(uri);

        try:
            if params:
                result = getattr(server,method)(params)
            else:
                result = getattr(server,method)()
        except Exception as err:
            result = str(err)
        return result

class xmlrpc_client:

    def __init__(self):
        self.builder = gtk.Builder()
        self.builder.add_from_file("xmlrpc-cli.glade")
        self.builder.connect_signals(self)
        self.builder.get_object("main_window").show_all()

    def on_main_window_destroy(self, widget):
        gtk.main_quit()

    def on_send_clicked(self, widget):
        uri = self.builder.get_object("uri").get_text()
        method = self.builder.get_object("method").get_text()
        params = self.builder.get_object("params").get_text()
        rpc = rpchelper()
        pretime = time.clock()
        result = rpc.call_server(uri, method, params)
        diff_time = time.clock() - pretime
        res_buffer = self.builder.get_object("response").get_buffer()
        res_buffer.set_text(str(result))
        self.builder.get_object("time").set_label(str(diff_time) + " s")

    def main(self):
        gtk.main()

# If the program is run directly or passed as an argument to the python
# interpreter then create an instance and show it
if __name__ == "__main__":
    xmlrpc_cli = xmlrpc_client()
    xmlrpc_cli.main()
